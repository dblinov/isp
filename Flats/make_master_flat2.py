#!/usr/bin/python
'''
This script searches for all flats in a given folder and produces
master flat-field files

Name must be like FF_R_2_5.fits where
R - a band name
2 - a series number (most likely we will do 2-3 series)
5 - a number of exposure in the current series (usually 3,5,7)
'''

import astropy.io.fits as pyfits
import numpy as np
import sys
import os

data_dir = './' #sys.argv[1]
mask_file_name = 'spider_mask.fits'
BIAS = 562

class FlatSeries():
   def __init__(self):
      self.filter_name = ''
      self.direction = ''
      self.series_number = None
      self.list_of_files = []
      self.list_of_data = []
      self.list_of_headers = []
      self.list_of_modes = []
      self.list_of_scaled_data = []
      self.median_mode = None
      self.master = None
      self.normalized_master = None
      self.header = None

   def getMasterName(self):
      return os.path.join(data_dir,'Flat_'+self.filter_name+'_'+self.direction+'_'+self.series_number+'.fits')

   def getNormMasterName(self):
      return os.path.join(data_dir,'Flat_Normalized'+self.filter_name+'_'+self.direction+'_'+self.series_number+'.fits')

   def getMedianMode(self):
      return np.median(self.list_of_modes)

   def scale(self):
      '''Scales all flat fields to make the mode of each image the same.
      Saves numpy array of 2D scaled arrays.
      '''
      for flat in self.list_of_data:
         self.list_of_modes.append(find_mode(flat))
      self.median_mode = self.getMedianMode()

      # Now normalize all other flats to one with median mode
      list_of_scaled_data = []
      for i in range(len(self.list_of_data)):
         list_of_scaled_data.append((self.median_mode/self.list_of_modes[i])*self.list_of_data[i])
      self.list_of_scaled_data = np.array(list_of_scaled_data)
 
   def makeMaster(self):
      self.scale()
      #self.master = np.round(np.median(self.list_of_scaled_data,axis=0)).astype(int)
      self.master = np.median(self.list_of_scaled_data,axis=0) - BIAS
      self.master[self.master <= 0] = 1
      self.header = self.list_of_headers[0]
      self.header['EXPOSURE'] = self.header['EXPTIME']*len(self.list_of_data)
      try:
         # Remove broken keyword
         del self.header['SWCREATE']
         del self.header[24]
      except:
         pass
      #write_fits(self.master,header,self.getMasterName())
      write_fits(np.round(self.master).astype(int), self.header, self.getMasterName())

   def makeNormalizedMaster(self, mask_data):
      maximum = max(self.master.flatten())
      self.normalized_master = self.master*(1.0/maximum)
      write_fits(self.normalized_master, self.header, self.getNormMasterName())
   

def getFindS(file_name):
   '''Return a tuple containing a filter name and
   series number for the given file name
   '''
   clean_file_name = os.path.splitext(file_name)[0] # filename without extension
   splitted_file_name = clean_file_name.split('_')
   filter_name = splitted_file_name[1]
   direction = splitted_file_name[2]
   series_number = splitted_file_name[3].split('-')[0]
   return filter_name, direction, series_number


def find_series():
   '''Searches for flat-field files in a directory
   and sorts them into series.
   Returns list of FlatSeries instances
   '''
   filelist = os.listdir(data_dir)
   filelist = filter(lambda x: not os.path.isdir(x), filelist)
   filelist = filter(lambda x: x.startswith('FF_'), filelist)

   # find all possible filter names and series numbers
   all_filters = []
   all_series = []
   all_directions = []
   for fn in filelist:
      filter_name, direction, series_number = getFindS(fn)
      all_filters.append(filter_name)
      all_directions.append(direction)
      all_series.append(series_number)

   filters = list(set(all_filters)) # leave only unique filter names
   directions = list(set(all_directions)) # leave only unique direction names
   series = list(set(all_series)) # leave only unique series numbers

   # now collect filenames of a certain series in a certain filter
   series_list = []
   series_list_identifiers = []
   for dir_n in directions:
      for filt in filters:
         for ser_n in series:
            cur_series_names = []
            cur_series_data = []
            cur_series_headers = []
            for fname in filelist:
               filter_name, direction, series_number = getFindS(fname)
               if filter_name == filt and series_number == ser_n and direction == dir_n:
                  full_name = os.path.join(data_dir,fname)
                  cur_series_names.append(full_name)
                  data, header = read_fits(full_name)
                  cur_series_data.append(data)
                  cur_series_headers.append(header)
            if len(cur_series_names) > 0:
               ser = FlatSeries()
               ser.filter_name = filt
               ser.direction = dir_n
               ser.series_number = ser_n
               ser.list_of_files = cur_series_names
               ser.list_of_data = cur_series_data
               ser.list_of_headers = cur_series_headers
               series_list.append(ser)
   return series_list


def read_fits(fn):
   '''Reading spider mask fits file'''
   FitsHDU    = pyfits.open(fn)
   FitsData   = FitsHDU[0].data
   FitsHeader = FitsHDU[0].header
   FitsHDU.close()
   return FitsData, FitsHeader

def write_fits(data,header,out_file_name):
   FitsHDU = pyfits.PrimaryHDU(data=data, header=header)
   if os.path.exists(out_file_name) : os.remove(out_file_name)
   FitsHDU.writeto(out_file_name)
   return 0

def find_mode(flat):
   '''Calculates the most frequent ADU value of a given flat.
   First we remove outliers by a sigma-clipping (this is the slowest part of the program).
   Then we make histogram of ADU. And smooth with floating mean through 5 points.
   Finally calculate peaking value of the histogram.
   It works now only with non-negative integer flats.
   '''
   flat = flat.flatten().astype(int)

   # Sigma clipping
   mean = np.mean(flat)
   sigma  = np.std(flat)
   #arr = filter(lambda x: x < mean+3*sigma and x > mean-3*sigma, flat)
   arr = flat                      #FIXME                              FIXME                                    FIXME

   # Make histogram (works only for non-negative integers)
   counts = np.bincount(arr)
   nbins = max(arr) + 1

   # Smooth the distribution
   smoothed_counts = [counts[0],counts[1]]
   for i in xrange(2,nbins-2):
      smoothed_counts.append(sum(counts[i-2:i+3])/5.0)

   return np.argmax(smoothed_counts)


if __name__ == "__main__":
   series_list = find_series()
   mask_data, mask_header = read_fits(mask_file_name)
   for series in series_list:
      series.makeMaster()
      series.makeNormalizedMaster(mask_data)

