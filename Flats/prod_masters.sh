#!/bin/bash


cat "dirs" |
while read l
do 
  echo $l
  cp generate_mask_outline.py $l
  cp make_master_flat2.py $l
  cp aver_flat.py $l
  cp fit_var2.py $l
  cd $l
  ./generate_mask_outline.py
  ./make_master_flat2.py
  ./aver_flat.py
  ./fit_var2.py
  cd ..
done
