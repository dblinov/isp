In this folder you can create master flats, flattened flats etc.
 
- Put folders with raw flats in the current folder. Each folder must contain flats taken at one night and named like
FF_R_E_1-001.fits, ..., FF_R_W_2-005.fits, ...
 
- run "ls -d */ | cut -f1 -d'/' > dirs" in the terminal

- run "./prod_masters.sh"

as the result in each folder following files will be produced:
Flat_R_E_1.fits - master flat (median of all shots) for series 1 of flates taken at West (i.e. evening flats)
Flat_NormalizedR_E_1.fits - same as previous but normalized, i.e. lowest pixel value = 0, highest = 1
Flat_R.fits - average of normalized flats of all series taken at night
spider_mask.fits - fits with a flag outlining the mask found in flats
mask_outline.pickle - outline of the mask stored in python pickle
flat_flat.fits - Flat_R.fits with subtracted gradient
flat_flat_norm.fits - normalized (min=0,max=1) version of flat_flat.fits
flatflats.png - shows the global vignetting model and fit residuals
