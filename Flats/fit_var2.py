#!/usr/bin/python

import numpy as np
from astropy.modeling import models, fitting
import matplotlib.pyplot as plt
import astropy.io.fits as pyfits
import os

filename = 'Flat_R.fits'
mask_outline = 'spider_mask.fits'
new_flat = 'flat_flat.fits'
new_flat_norm = 'flat_flat_norm.fits'
pngfilename = "flatflats.png"

def getMinMax(arr):
    pos_ind = np.where(arr > 0.5)
    mn,mx = np.min(pos_ind),np.max(pos_ind)
    return mn,mx

def findCross():
    HDU    = pyfits.open(mask_outline)
    MaskOutlineData   = HDU[0].data
    HDU.close()
    xl1,xr1 = getMinMax(MaskOutlineData[0,:])
    xl2,xr2 = getMinMax(MaskOutlineData[2047,:])
    xl = min(xl1,xl2)
    xr = max(xr1,xr2)
    yb1,yt1 = getMinMax(MaskOutlineData[:,0])
    yb2,yt2 = getMinMax(MaskOutlineData[:,2047])
    yb = min(yb1,yb2)
    yt = max(yt1,yt2)
    return xl-1,xr+1,yb-1,yt+1

def getSurface():
    xl,xr,yb,yt = findCross()
    FitsHDU    = pyfits.open(filename)
    FitsData   = FitsHDU[0].data
    FitsHeader = FitsHDU[0].header
    FitsHDU.close()
    leny = len(FitsData)
    lenx = len(FitsData[0])
    xg = [i for i in range(xl)] + [i for i in range(xr,lenx)]
    yg = [i for i in range(yb)] + [i for i in range(yt,leny)]
    x,y = np.meshgrid(xg,yg)
    m = []
    for k in range(2048):
        row = []
        for i in range(2048):
            el = False
            if xl <= i < xr or yb <= k < yt:
                el = True
            row.append(el)
        m.append(row)
    mask = np.ma.make_mask(m)
    MaskedFitsData = np.ma.MaskedArray(FitsData, mask=mask, copy=True)
    MFD = np.array(MaskedFitsData[~MaskedFitsData.mask].reshape(len(yg),len(xg)))

    return x, y, MFD, FitsData

x, y, z, zfull = getSurface()
xfull, yfull = np.meshgrid([i for i in range(2048)],[i for i in range(2048)])

p_init = models.Poly2DModel(degree=3)

f = fitting.NonLinearLSQFitter(p_init)
f(x, y, z)

new_flat_data = zfull - p_init(xfull, yfull)

def writeFits(data,fn):
    if os.path.exists(fn) : os.remove(fn)
    hdu = pyfits.PrimaryHDU(data.astype(np.float32))
    hdulist = pyfits.HDUList([hdu])
    hdulist.writeto(fn)
    hdulist.close()

writeFits(new_flat_data,new_flat)

# normalize new flat
def makeNormalizedFlat(flat):
    maximum = max(flat.flatten())
    minimum = min(flat.flatten())
    fact = 1.0/(maximum-minimum)
    flat_norm = flat - minimum
    return flat_norm*fact

new_flat_norm_data = makeNormalizedFlat(new_flat_data)
writeFits(new_flat_norm_data,new_flat_norm)

# plot graphs
plt.figure(figsize=(15,15))

plt.subplot(2,2,1)
cax = plt.imshow(zfull, interpolation='nearest', vmin=0.7, vmax=0.98)
plt.colorbar(cax, orientation='horizontal',fraction=0.07,ticks=[0.73,0.84,0.95])
plt.title("Data")

plt.subplot(2,2,2)
cax2 = plt.imshow(p_init(xfull, yfull), interpolation='nearest', vmin=0.7, vmax=0.98)
plt.colorbar(cax2, orientation='horizontal',fraction=0.07,ticks=[0.73,0.84,0.95])
plt.title("Model")

plt.subplot(2,2,3)
cax3 = plt.imshow(new_flat_data, interpolation='nearest', vmin=-0.01, vmax=0.06)
plt.colorbar(cax3, orientation='horizontal',fraction=0.07,ticks=[0.0,0.025,0.05])
plt.title("Residual")

plt.subplot(2,2,4)
cax4 = plt.imshow(new_flat_norm_data, interpolation='nearest', vmin=0.65, vmax=0.85)
plt.colorbar(cax4, orientation='horizontal',fraction=0.07,ticks=[0.70,0.75,0.80])
plt.title("New Norm. Flat")
plt.savefig(pngfilename,bbox_inches='tight')

