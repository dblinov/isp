#!/usr/bin/python
'''
This script searches for all flats in a given folder and produces
master flat-field files

Name must be like FF_R_2_5.fits where
R - a band name
2 - a series number (most likely we will do 2-3 series)
5 - a number of exposure in the current series (usually 3,5,7)
'''

import astropy.io.fits as pyfits
import numpy as np
import sys
import os
import shutil 

data_dir = './' #sys.argv[1]
out_file_name = 'Flat_R.fits'

def find_files():
   '''Searches for flat-field files in a directory
   and sorts them into series.
   Returns list of FlatSeries instances
   '''
   filelist = os.listdir(data_dir)
   filelist = filter(lambda x: not os.path.isdir(x), filelist)
   filelist = filter(lambda x: x.startswith('Flat_NormalizedR_'), filelist)

   return filelist


def read_fits(fn):
   '''Reading spider mask fits file'''
   FitsHDU    = pyfits.open(fn)
   FitsData   = FitsHDU[0].data
   FitsHeader = FitsHDU[0].header
   return FitsData, FitsHeader


def write_fits(data,header,out_file_name):
   FitsHDU = pyfits.PrimaryHDU(data=data, header=header)
   if os.path.exists(out_file_name) : os.remove(out_file_name)
   FitsHDU.writeto(out_file_name)
   return 0

def calc_average(filelist):
  if len(filelist) == 1:
    shutil.copyfile(filelist[0], out_file_name)
    return
  datalist = []
  for fn in filelist:
    FitsData, FitsHeader = read_fits(fn)
    datalist.append(FitsData)
  FitsDataNew = np.median(datalist,axis=0)
  write_fits(FitsDataNew,FitsHeader,out_file_name)

if __name__ == "__main__":
  filelist = find_files()
  calc_average(filelist)
