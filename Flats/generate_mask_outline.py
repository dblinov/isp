#!/usr/bin/python

import numpy as np
import astropy.io.fits as pyfits
import matplotlib.pyplot as plt
import scipy.signal as signal
import ds9 as ds9
import pickle
import os

data_dir = './'

def find_flat():
   '''Searches for flat-field files in a directory
   Returns a random (1st) file
   '''
   filelist = os.listdir(data_dir)
   filelist = filter(lambda x: not os.path.isdir(x), filelist)
   filelist = filter(lambda x: x.startswith('FF_'), filelist)
   return filelist[0]

ftest = find_flat()
image = pyfits.open(ftest)[0].data

def find_edges(row):
    low = 850
    high = 1200
    length = 33
    w = 35
    window = np.append( -1*np.ones(length/2), np.append(np.ones(length),  -1*np.ones(length/2)))
    selection = np.convolve(row,window,'same')[low:high]
    peaks = []
    Iinclude = np.ones(selection.shape) == 1
    indarr = np.arange(low,high)
    for k in range(3):
        # find the index of the maximum:
        Imax = indarr[Iinclude][np.argmax(selection[Iinclude])]
        peaks.append(Imax)
        # find the index of the minimum:
        Imin = indarr[Iinclude][np.argmin(selection[Iinclude])]
        peaks.append(Imin)
        # exclude these sources from consideration the next time:
        Iinclude[ np.abs(indarr - Imax) < length/2 ] = False
        Iinclude[ np.abs(indarr - Imin) < length/2 ] = False
    # and get one more edge: a maximum:
    Imax = indarr[Iinclude][np.argmax(selection[Iinclude])]
    peaks.append(Imax)
    return np.sort(np.array(peaks))

row_num = np.arange(1,2049)
vert_edges = np.zeros((2048,7))
horz_edges = np.zeros((2048,7))
for k in row_num:
    row = image[k-1,:]
    vert_edges[k-1,:] = find_edges(row)
    col = image[:,k-1]
    horz_edges[k-1,:] = find_edges(col)
# so the vertical edges are:
left_edge_l = vert_edges[:,0]
left_edge_r = vert_edges[:,1]
left_edge = (left_edge_l + left_edge_r)/2.0
right_edge_l = vert_edges[:,5]
right_edge_r = vert_edges[:,6]
right_edge = (right_edge_l + right_edge_r)/2.0

# and the horizontal edges are:
bottom_edge_l = horz_edges[:,0]
bottom_edge_r = horz_edges[:,1]
bottom_edge = (bottom_edge_l + bottom_edge_r)/2.0
top_edge_l = horz_edges[:,5]
top_edge_r = horz_edges[:,6]
top_edge = (top_edge_l + top_edge_r)/2.0

# and now perform outlier rejection:
left_edge = signal.medfilt(left_edge,kernel_size=51)
right_edge = signal.medfilt(right_edge,kernel_size=51)
bottom_edge = signal.medfilt(bottom_edge,kernel_size=51)
top_edge = signal.medfilt(top_edge,kernel_size=51)

Iplot = ((row_num > 50) & (row_num < 840)) | ((row_num > 1250) & (row_num < 1998))
Pleft = np.polyfit(row_num[Iplot],left_edge[Iplot],3)
Pright = np.polyfit(row_num[Iplot],right_edge[Iplot],3)
Pbottom = np.polyfit(row_num[Iplot],bottom_edge[Iplot],3)
Ptop = np.polyfit(row_num[Iplot],top_edge[Iplot],3)
plt.figure(figsize=(6,6))
plt.plot(left_edge[Iplot],row_num[Iplot],'k.',
         np.polyval(Pleft,row_num),row_num,'r-',
         right_edge[Iplot],row_num[Iplot],'k.',
         np.polyval(Pright,row_num),row_num,'r-',
         row_num[Iplot],bottom_edge[Iplot],'g.',
         row_num,np.polyval(Pbottom,row_num),'b-',
         row_num[Iplot],top_edge[Iplot],'g.',
         row_num,np.polyval(Ptop,row_num),'b-')
plt.axis('equal')
plt.title('Detected edges of the mask: nominal mask pattern')
plt.xlim([1,2048])
plt.ylim([1,2048])

# save this to disk:
lfit = np.polyval(Pleft,row_num)
rfit = np.polyval(Pright,row_num)
bfit = np.polyval(Pbottom,row_num)
tfit = np.polyval(Ptop,row_num)

lline = np.round(lfit).astype(int)-1
rline = np.round(rfit).astype(int)-1
bline = np.round(bfit).astype(int)-1
tline = np.round(tfit).astype(int)-1
mask_outline = {'lline':lline,'rline':rline,'bline':bline,'tline':tline,
                'lfit':lfit,'rfit':rfit,'bfit':bfit,'tfit':tfit,
                'pixel_num':row_num}
pickle.dump(mask_outline,open('mask_outline.pickle','w'))

# create fits file with mask
data = np.zeros(shape=(2048,2048))
for i in range(2048):
    l = lline[i]
    r = rline[i]
    data[i,l:r] = 1
    b = bline[i]
    t = tline[i]
    data[b:t,i] = 1
if os.path.exists('spider_mask.fits') : os.remove('spider_mask.fits')
hdu = pyfits.PrimaryHDU(data)
hdulist = pyfits.HDUList([hdu])
hdulist.writeto('spider_mask.fits')
hdulist.close()


