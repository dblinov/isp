#!/usr/bin/python

import MySQLdb
import datetime
import os,shutil
import numpy as np
from pipeline import robopol_pipeline
import math
import subprocess
import dbdbauth
import time

fb_host   = dbdbauth.fb_host
fb_db     = dbdbauth.fb_db
fb_user   = dbdbauth.fb_user
fb_passwd = dbdbauth.fb_passwd

rb_host   = dbdbauth.rb_host
rb_db     = dbdbauth.rb_db
rb_user   = dbdbauth.rb_user
rb_passwd = dbdbauth.rb_passwd

DATA_DIR = os.environ['ROBOPOL_DATA_DIR']
OBJ_PREFIX = "PF"
RESULT_DIR = "/home/panopg/PF/"
SUPPL_DATA_FILE = '/home/panopg/PF.dat'

def Log(msg):
    fop = open('proc_PF.log','a')
    fop.write(datetime.datetime.utcnow().strftime("%d.%m %H:%M:%S ")+str(msg)+"\n")
    fop.close()

class fits_file():
    def __init__(self):
        self.date = None
        self.jdate = None
        self.blazar = None
        self.file = ""
        self.exp = 0

def getBlazarNames():
    """ returns unique blazar names in the raw files db """
    db = MySQLdb.connect(host=fb_host, user=fb_user, passwd=fb_passwd, db=fb_db)
    cursor = db.cursor()
    sql = "SELECT DISTINCT(OBJECT) FROM robopol WHERE IMAGETYP = 'science' AND FILTER = 'R' AND OBJECT like '"+OBJ_PREFIX+"%' ORDER BY OBJECT ASC"
    cursor.execute(sql)
    data = cursor.fetchall()
    blazars = []
    for line in data:
        obj_name = line[0]
        blazars.append(obj_name)
    db.close()
    return blazars

def getRawData(blazar):
    """ returns array of fits_file instances with all shots ever made for the given blazar name"""
    db = MySQLdb.connect(host=fb_host, user=fb_user, passwd=fb_passwd, db=fb_db)
    cursor = db.cursor()
    # select unique Julian dates when we have observations for this blazar
    sql = "SELECT file,`DATE-OBS`,JD,EXPOSURE FROM robopol WHERE FILTER = 'R' AND OBJECT = '%s'" % (blazar)
    cursor.execute(sql)
    data = cursor.fetchall()
    files = []
    for line in data:
        ff = fits_file()
        ff.blazar = blazar
        ff.file = line[0]
        ff.date = line[1]
        ff.jdate = line[2]
        ff.exp = line[3]
        files.append(ff)
    db.close()
    print len(files)
    return files

def getModelDate(jd):
    if jd < 2456460:
        return '2013_05_24'
    elif 2456460 < jd < 2456508:
        return '2013_06_20'
    elif 2456508 < jd < 2456567:
        return '2013_08_04'
    elif 2456567 < jd:
        return '2013_10_23'

def runPipeleine(raw_data_list):
    for fits_file in raw_data_list:
        Log('%s %s %s images processing ...'%(fits_file.blazar,fits_file.date,fits_file.file))
        output_dir = "/tmp/"+fits_file.blazar+"_"+fits_file.date
        try:
            fits_location = os.path.join(DATA_DIR,fits_file.file)  
            bll = robopol_pipeline.robopol_image(name=fits_file.blazar,fits_location=fits_location,output_dir=output_dir,photom_method=2,
                                     ecut=0.5,SNRcut=1,plot_images=0,prefix='robopol_stacked/robopol_',
                                     stacking_method='sum',imaging=False,align=True,flat_field=False,bias=562,stack_lib='alipy',
                                     use_target_fwhm=False,model_date='2014_04_30')
            bll.reduce_polarization()
            # copy result file
            basename = os.path.basename(fits_file.file).rstrip(".fits")
            out_filename = basename+".csv"
            shutil.copy(os.path.join(output_dir,'robopol_stacked/robopol_data.csv'),os.path.join(RESULT_DIR,out_filename))
            fop = open(SUPPL_DATA_FILE,'a')
            fop.write(fits_file.blazar+" "+fits_file.date+" "+str(fits_file.jdate)+" "+str(fits_file.exp)+" "+out_filename+"\n")
            fop.close()
        except:
            Log('Failed on %s %s %s'%(fits_file.blazar,fits_file.date,fits_file.file))
            shutil.rmtree(output_dir)
            continue
        shutil.rmtree(output_dir)

if __name__ == "__main__":
    # clean output file
    fop = open(SUPPL_DATA_FILE,'w')
    fop.close()
    t1 =  time.time()
    blazar_names = getBlazarNames()
    t2 = time.time() - t1
    print t2
    print blazar_names
    raw_input('w')
    for blazar in blazar_names:
        try:
            raw_data_list = getRawData(blazar)
            runPipeleine(raw_data_list)
        except:
            pass

