#!/usr/bin/python

import os
import numpy as np
import matplotlib.pyplot as plt

pcorr = 7.893
perrcorr = 0.037
evpacorr = 116.23
evpaerrcorr = 0.14

aperts = []
evpas = []
evpa_errs = []
ps = []
p_errs = []
t_0_f = []
t_0_f_err = []
t_1_f = []
t_1_f_err = []
t_2_f = []
t_2_f_err = []
t_3_f = []
t_3_f_err = []
dirs = os.listdir('./')
for dirname in dirs:
    if os.path.isdir(dirname):
        apert = float(dirname)
        fop = open(dirname+'/robopol_data.csv')
        fop.readline()
        fop.readline()
        fop.readline()
        line = fop.readline()
        sl = line.split(',')
        p = float(sl[4])
        p_err = float(sl[5])
        evpa = np.degrees(float(sl[6]))
        evpa_err = np.degrees(float(sl[7]))
        if evpa < 0:
            evpa = evpa + 180
        fop.close()
        aperts.append(apert)
        ps.append(p)
        p_errs.append(p_err)
        evpas.append(evpa)
        evpa_errs.append(evpa_err)
        # counts
        fop = open(dirname+'/robopol_targ_counts.dat')
        fop.readline()
        l0=fop.readline().rstrip('\n')
        sl0 = l0.split()
        t_0_f.append(float(sl0[1]))
        t_0_f_err.append(float(sl0[2]))
        #
        l1=fop.readline().rstrip('\n')
        sl1 = l1.split()
        t_1_f.append(float(sl1[1]))
        t_1_f_err.append(float(sl1[2]))
        #
        l2=fop.readline().rstrip('\n')
        sl2 = l2.split()
        t_2_f.append(float(sl2[1]))
        t_2_f_err.append(float(sl2[2]))
        #
        l3=fop.readline().rstrip('\n')
        sl3 = l3.split()
        t_3_f.append(float(sl3[1]))
        t_3_f_err.append(float(sl3[2]))
        fop.close()


plt.errorbar(aperts, ps, xerr=0, yerr=p_errs, fmt='o')
plt.xlabel('Aperture (Factor FWHM)')
plt.ylabel('P (fraction)')
plt.axhline(y=1e-2*(pcorr+perrcorr), linewidth=2, color = 'b')
plt.axhline(y=1e-2*(pcorr), linewidth=2, color = 'k')
plt.axhline(y=1e-2*(pcorr-perrcorr), linewidth=2, color = 'b')
plt.savefig(os.getcwd().split('/')[-1]+'_p.png')
plt.clf()
plt.close()
plt.cla()


plt.errorbar(aperts, evpas, xerr=0, yerr=evpa_errs, fmt='o')
plt.xlabel('Aperture (Factor FWHM)')
plt.ylabel('EVPA (deg)')
plt.axhline(y=(evpacorr+evpaerrcorr), linewidth=2, color = 'b')
plt.axhline(y=evpacorr, linewidth=2, color = 'k')
plt.axhline(y=(evpacorr-evpaerrcorr), linewidth=2, color = 'b')
plt.savefig(os.getcwd().split('/')[-1]+'_evpa.png')
plt.clf()
plt.close()
plt.cla()

# fit polynomials
def get_opt(x,y,w):
    k = 0.01
    x_cut = []
    y_cut = []
    w_cut = []
    for i in range(len(x)):
        if 2 <= x[i] <= 5.5:
            x_cut.append(x[i])
            y_cut.append(y[i])
            w_cut.append(w[i])
    p = np.polyfit(x_cut,y_cut,4,w=w_cut)
    a = p[0]
    b = p[1]
    c = p[2]
    d = p[3]
    e = p[4]
    
    p_der = [k*a,4*a-k*b,3*b-k*c,2*c-k*d,d-k*e]
    roots = np.roots(p_der)
    for r in roots:
        if r.imag == 0 and 2 <= r.real <= 6:
            opt = r.real
    return opt,p

'''
def get_opt2(x,y,w):
    gr_end = 0.02
    x_cut = []
    y_cut = []
    w_cut = []
    for i in range(len(x)):
        if 2 <= x[i] <= 7:
            x_cut.append(x[i])
            y_cut.append(y[i])
            w_cut.append(w[i])
    p = np.polyfit(x_cut,y_cut,3,w=w_cut)
    p_der = [3*p[0],2*p[1],p[2]-gr_end]
    roots = np.roots(p_der)
    
    for r in roots:
        if 2 <= r <= 6:
            opt = r
    return opt,p

def get_opt1(x,y,w):
    gr_end = 0.02
    x_cut = []
    y_cut = []
    w_cut = []
    for i in range(len(x)):
        if 2 <= x[i] <= 7:
            x_cut.append(x[i])
            y_cut.append(y[i])
            w_cut.append(w[i])
    p = np.polyfit(x_cut,y_cut,2,w=w_cut)
    opt = (gr_end - p[1])/2.0/p[0]
    return opt,p
'''

opt0,p0 = get_opt(aperts,t_0_f,map(lambda x: 1.0/x,t_0_f_err))
polynomial = np.poly1d(p0)
xs = np.arange(2,5.5,0.1)
ys = polynomial(xs)
plt.axvline(x=opt0,color='g')
plt.plot(xs, ys, 'g-')
plt.errorbar(aperts, t_0_f, xerr=0, yerr=t_0_f_err, fmt='o', c='g')

opt1,p1 = get_opt(aperts,t_1_f,map(lambda x: 1.0/x,t_1_f_err))
polynomial = np.poly1d(p1)
xs = np.arange(2,5.5,0.1)
ys = polynomial(xs)
plt.axvline(x=opt1,color='b')
plt.plot(xs, ys, 'b-')
plt.errorbar(aperts, t_1_f, xerr=0, yerr=t_1_f_err, fmt='o', c='b')

opt2,p2 = get_opt(aperts,t_2_f,map(lambda x: 1.0/x,t_2_f_err))
polynomial = np.poly1d(p2)
xs = np.arange(2,5.5,0.1)
ys = polynomial(xs)
plt.axvline(x=opt2,color='r')
plt.plot(xs, ys, 'r-')
plt.errorbar(aperts, t_2_f, xerr=0, yerr=t_2_f_err, fmt='o', c='r')

opt3,p3 = get_opt(aperts,t_3_f,map(lambda x: 1.0/x,t_3_f_err))
polynomial = np.poly1d(p3)
xs = np.arange(2,5.5,0.1)
ys = polynomial(xs)
plt.axvline(x=opt3,color='k')
plt.plot(xs, ys, 'k-')
plt.errorbar(aperts, t_3_f, xerr=0, yerr=t_3_f_err, fmt='o', c='k')
plt.xlabel('Aperture (Factor FWHM)')
plt.ylabel('Flux')
plt.savefig(os.getcwd().split('/')[-1]+'_gc.png')
plt.clf()
plt.close()
plt.cla()
